import { ArgumentsHost, Catch, HttpException, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { EnvironmentTypes } from '../types/environment.type';
import { Response } from 'express';

@Catch(HttpException)
export class ExceptionFilter {
  private isDebug: boolean;

  constructor(readonly configService: ConfigService) {
    const env = configService.get<string>('nodeEnv') as EnvironmentTypes;

    this.isDebug = [
      EnvironmentTypes.DEVELOPMENT,
      EnvironmentTypes.STAGING,
    ].includes(env);
  }

  catch(exception: HttpException, host: ArgumentsHost) {
    if (this.isDebug) {
      Logger.error(exception, exception.stack, 'ExceptionFilter');
      Logger.verbose(JSON.stringify(exception, null, 2), 'ExceptionFilter');
    }

    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const status = exception.getStatus();
    const message =
      (exception.getResponse() as any).message || exception.getResponse();

    response.status(status).send({
      statusCode: status,
      timestamp: new Date(),
      message: typeof message == 'string' ? message : message[0],
    });
  }
}
