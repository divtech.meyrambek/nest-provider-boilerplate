import { Injectable } from '@nestjs/common';
import { UserEntityService } from 'src/infrastructure/database/services/users-entity.service';
import { RedisConfigService } from 'src/infrastructure/redis-config/service';
import { CreateUserDto } from './dto';
import { UserTypeormEntity } from 'src/infrastructure/database/typeorm-entities/users.typeorm-entity';

@Injectable()
export class UserService {
  constructor(
    private redisConfigService: RedisConfigService,
    private userEntityService: UserEntityService,
  ) {}

  async create(input: CreateUserDto): Promise<UserTypeormEntity> {
    const user = await this.userEntityService.store(input);
    delete user.password;
    await this.redisConfigService.save(`user-id-${user.id}`, user);
    const hashedUser = await this.redisConfigService.getValueByKey(
      `user-id-${user.id}`,
    );
    console.log(hashedUser);
    return user;
  }
}
