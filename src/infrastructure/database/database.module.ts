import { Global, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import { EnvironmentTypes } from '../types/environment.type';
import { UserTypeormEntity } from './typeorm-entities/users.typeorm-entity';
import { UserEntityService } from './services/users-entity.service';

@Global()
@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: 'mysql',
        host: configService.get<string>('database.host'),
        port: configService.get<number>('database.port'),
        username: configService.get<string>('database.user'),
        password: configService.get<string>('database.password'),
        database: configService.get<string>('database.dbName'),
        timezone: configService.get<string>('timeZone'),
        entities: [__dirname + '/../**/*.entity.{js,ts}'],
        autoLoadEntities: true,
        synchronize: true,
        namingStrategy: new SnakeNamingStrategy(),
        logging:
          configService.get<string>('nodeEnv') === EnvironmentTypes.DEVELOPMENT
            ? true
            : false,
      }),
      inject: [ConfigService],
    }),
    TypeOrmModule.forFeature([UserTypeormEntity]),
  ],
  providers: [UserEntityService],
  exports: [UserEntityService],
})
export class DatabaseModule {}
