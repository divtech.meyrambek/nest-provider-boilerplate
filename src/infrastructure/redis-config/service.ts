import { RedisService } from '@liaoliaots/nestjs-redis';
import { Injectable } from '@nestjs/common';
import * as Redis from 'ioredis';

@Injectable()
export class RedisConfigService {
  private readonly redisClient: Redis.Redis;

  constructor(private readonly redisService: RedisService) {
    this.redisClient = redisService.getClient();
  }

  async save(key: string, value: any): Promise<void> {
    await this.redisClient.setex(key, 7699000, JSON.stringify(value));
  }

  async getValueByKey(key: string): Promise<any> {
    const value = await this.redisClient.get(key);
    if (!value) return null;
    return JSON.parse(value);
  }
}
