import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserTypeormEntity } from '../typeorm-entities/users.typeorm-entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';

@Injectable()
export class UserEntityService {
  constructor(
    @InjectRepository(UserTypeormEntity)
    private userRepository: Repository<UserTypeormEntity>,
  ) {}

  async store(payload: Partial<UserTypeormEntity>): Promise<UserTypeormEntity> {
    return await this.userRepository.save(this.userRepository.create(payload));
  }

  async findOne(id: number): Promise<UserTypeormEntity> {
    return await this.userRepository.findOne({ where: { id } });
  }

  async update(
    id: number,
    payload: Partial<UserTypeormEntity>,
  ): Promise<UpdateResult> {
    return await this.userRepository.update(id, payload);
  }

  async delete(id: number): Promise<DeleteResult> {
    return await this.userRepository.softDelete(id);
  }
}
