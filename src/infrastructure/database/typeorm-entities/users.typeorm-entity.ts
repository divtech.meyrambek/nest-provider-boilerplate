import { BeforeInsert, BeforeUpdate, Column, Entity } from 'typeorm';
import { PersonTypeormEntity } from './person.typeorm-entity';
import * as bcrypt from 'bcryptjs';

const tableName = 'users';

@Entity({ name: tableName })
export class UserTypeormEntity extends PersonTypeormEntity {
  @Column({ type: 'text', nullable: false })
  email!: string;

  @Column({ type: 'text', nullable: false, select: false })
  password!: string;

  @BeforeInsert()
  async hashPasswordBeforeInsert() {
    this.password = await bcrypt.hash(this.password, await bcrypt.genSalt());
  }

  @BeforeUpdate()
  async hashPasswordBeforeUpdate() {
    if (this.password) {
      this.password = await bcrypt.hash(this.password, await bcrypt.genSalt());
    }
  }
}
