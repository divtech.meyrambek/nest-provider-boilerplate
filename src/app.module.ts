import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { ConfigModule } from '@nestjs/config';
import { DatabaseModule } from './infrastructure/database/database.module';
import {
  loadConfiguration,
  validationSchema,
} from './infrastructure/configs/environment.config';
import { RedisConfigModule } from './infrastructure/redis-config/redis-config.module';
import { UsersModule } from './modules/users/users.module';
@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [loadConfiguration],
      validationSchema: validationSchema,
      validationOptions: { abortEarly: true },
    }),
    DatabaseModule,
    RedisConfigModule,
    UsersModule,
  ],
  controllers: [AppController],
})
export class AppModule {}
