import { Body, Controller, Post } from '@nestjs/common';
import { UserService } from './service';
import { CreateUserDto } from './dto';
import { UserTypeormEntity } from 'src/infrastructure/database/typeorm-entities/users.typeorm-entity';

@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  @Post()
  create(@Body() input: CreateUserDto): Promise<UserTypeormEntity> {
    return this.userService.create(input);
  }
}
