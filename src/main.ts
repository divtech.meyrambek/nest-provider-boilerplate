import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { EnvironmentTypes } from './infrastructure/types/environment.type';
import { Logger, ValidationPipe } from '@nestjs/common';
import { useContainer } from 'class-validator';
import { json as expressJson, urlencoded as expressUrlEncoded } from 'express';
import { ExceptionFilter } from './infrastructure/filters/exception.filter';
import { setupSwagger } from './infrastructure/plugins/swagger.plugin';

async function bootstrap() {
  try {
    const app = await NestFactory.create(AppModule);
    const configService = app.get(ConfigService);
    const env = configService.get<string>('nodeEnv');
    const port = configService.get<number>('port') as number;
    const corsClientUrls = configService.get<string[]>('corsClientUrls');

    app.enableCors({
      origin:
        env === EnvironmentTypes.DEVELOPMENT || env === EnvironmentTypes.STAGING
          ? '*'
          : corsClientUrls,
      credentials:
        env === EnvironmentTypes.DEVELOPMENT || env === EnvironmentTypes.STAGING
          ? false
          : true,
    });

    app.setGlobalPrefix('api');

    app.useGlobalPipes(
      new ValidationPipe({ transform: true, stopAtFirstError: true }),
    );

    app.useGlobalFilters(new ExceptionFilter(configService));

    if (
      env === EnvironmentTypes.DEVELOPMENT ||
      env === EnvironmentTypes.STAGING
    ) {
      setupSwagger(app, configService);
    } else {
      Logger.debug = () => {
        //disabled in production
      };
      Logger.verbose = () => {
        //disabled in production
      };
    }

    useContainer(app.select(AppModule), {
      fallbackOnErrors: true,
    });

    app.use(expressJson({ limit: '50mb' }));
    app.use(expressUrlEncoded({ limit: '50mb', extended: true }));

    try {
      await app.listen(port, '0.0.0.0', () => {
        Logger.log(`Running on port ${port}`, 'NestApplication');
        Logger.log(`Environment ${env}`, 'NestApplication');
      });
    } catch (error) {
      Logger.error(`Error occurred while starting the application: ${error}`);
    }
  } catch (error) {
    Logger.log('Error occurred during application bootstrap:', error);
  }
}
bootstrap();
