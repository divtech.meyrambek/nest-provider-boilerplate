import { INestApplication } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  DocumentBuilder,
  SwaggerCustomOptions,
  SwaggerModule,
} from '@nestjs/swagger';
import * as basicAuth from 'express-basic-auth';

export const setupSwagger = (
  app: INestApplication,
  configService: ConfigService,
) => {
  const user = configService.get<string>('swagger.user') as string;
  const password = configService.get<string>('swagger.password') as string;

  app.use(
    ['/swagger', '/swagger-json'],
    basicAuth({
      challenge: true,
      users: {
        [user]: password,
      },
    }),
  );
  const options = new DocumentBuilder()
    .setTitle('Provider Boilerplate')
    .setDescription('The Provider Boilerplate API documentation')
    .setVersion('1.0')
    .addBearerAuth(
      {
        type: 'http',
        scheme: 'bearer',
        bearerFormat: 'JWT',
        name: 'JWT access token',
        description: 'Enter JWT token',
        in: 'header',
      },
      'JWT-auth',
    )
    .build();

  const document = SwaggerModule.createDocument(app, options);

  const customOptions: SwaggerCustomOptions = {
    customSiteTitle: 'Provider Boilerplate documentation',
  };

  SwaggerModule.setup('swagger', app, document, customOptions);
};
