import * as Joi from 'joi';
import { EnvironmentTypes } from '../types/environment.type';
export const loadConfiguration = (): Record<string, unknown> => {
  return {
    nodeEnv: process.env.NODE_ENV as EnvironmentTypes,
    port: process.env.PORT,
    corsClientUrls: process.env.CORS_CLIENT_URLS?.split(','),
    timeZone: process.env.TZ,
    database: {
      host: process.env.DATABASE_HOST,
      port: parseInt(process.env.DATABASE_PORT as string),
      user: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
      dbName: process.env.DATABASE_NAME,
    },
    redis: {
      common: {
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT,
      },
      expiresIn: process.env.REDIS_EXPIRES_IN,
    },
    swagger: {
      user: process.env.SWAGGER_USER,
      password: process.env.SWAGGER_PASSWORD,
    },
  };
};

export const validationSchema = Joi.object({
  //ENV
  NODE_ENV: Joi.string()
    .valid(
      EnvironmentTypes.DEVELOPMENT,
      EnvironmentTypes.STAGING,
      EnvironmentTypes.PRODUCTION,
    )
    .required(),
  PORT: Joi.number().required(),
  CORS_CLIENT_URLS: Joi.string().required(),

  //TIMEZONE
  TZ: Joi.string().required(),

  //DATABASE
  DATABASE_HOST: Joi.string().required(),
  DATABASE_PORT: Joi.number().required(),
  DATABASE_USER: Joi.string().required(),
  DATABASE_PASSWORD: Joi.string().required(),
  DATABASE_NAME: Joi.string().required(),

  // REDIS
  REDIS_HOST: Joi.string().required(),
  REDIS_PORT: Joi.number().required(),
  REDIS_EXPIRES_IN: Joi.string().required(),

  //SWAGGER
  SWAGGER_USER: Joi.string().required(),
  SWAGGER_PASSWORD: Joi.string().required(),
});
